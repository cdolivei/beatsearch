/* vim: set expandtab shiftwidth=8 tabstop=8 */
define([ "jquery", "q", "beatport" ], function($, Q, Beatport) {
        return {
                get : function(id)
                {
                        if (isNaN(parseInt(id, 10)))
                        {
                                throw new Error(query + " is not a number");
                        }
                        return Beatport.request("/catalog/3/artists/detail", {
                                id : id
                        });
                },
                search : function(query, page)
                {
                        return Beatport.request("/catalog/search", {
                                facets : "fieldType:performer",
                                query : query,
                                page : page || 1,
                                perPage : 20
                        }).then(function(data) {
                                // We need to save the query in case the user decides
                                // to go view page 2,3,etc
                                data.metadata.savedQuery = query;
                                return data;
                        });
                },

                tracks_search : function(id, page, filter)
                {
                        if (isNaN(parseInt(id, 10)))
                        {
                                throw new Error(id + " is not a number");
                        }

                        var request = null;

                        if (typeof(filter) === 'undefined')
                        {
                                request = Beatport.request("/catalog/3/tracks", {
                                                           facets : "artistId:" + id,
                                                           page : page || 1,
                                                           perPage : 50
                                });
                        }
                        else
                        {
                                request = Beatport.request("/catalog/search", {
                                                           'facets' : [ "artistId:" + id, "fieldType:track" ],
                                                           query : filter,
                                                           page : page || 1,
                                                           perPage : 50
                                });

                        }
                        return request.then(function(data) {
                                data.metadata.savedQuery = id;
                                return data;
                        });
                },

                releases_search : function(id, page, filter)
                {
                        if (isNaN(parseInt(id, 10)))
                        {
                                throw new Error(query + " is not a number");
                        }

                        var request = null;

                        if (typeof(filter) === 'undefined')
                        {

                                request = Beatport.request("/catalog/3/releases", {
                                                           facets : "artistId:" + id,
                                                           page : page || 1,
                                                           perPage : 30
                                });
                        }
                        else
                        {
                                request = Beatport.request("/catalog/search", {
                                                           facets : [ "artistId:" + id, "fieldType:release" ],
                                                           query : filter,
                                                           page : page || 1,
                                                           perPage : 30
                                });
                        }
                        return request.then(function(data) {
                                data.metadata.savedQuery = id;
                                return data;
                        });
                }

        };
});
