/* vim: set expandtab shiftwidth=8 tabstop=8 */
define([ "jquery", "q", "beatport" ], function($, Q, Beatport) {
        return {
                search : function(query, page)
                {
                        return Beatport.request("/catalog/search", {
                                query : query,
                                page : page || 1,
                                perPage : 40
                        }).then(function(data) {
                                data.metadata.savedQuery = query;
                                return data;
                        });
                }
        };
});
