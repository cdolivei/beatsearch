/* vim: set expandtab shiftwidth=8 tabstop=8 */
define([ 'mustache', 'jquery', 'jquery.jplayer' ], function(Mustache, $, jPlayer) {
        return {
                ready : false,
                onerror : null,

                init : function(error_callback) {
                        var template = $("#jplayer_template").html();
                        var self = this;
                        this.error = error_callback;
                        $('#jplayer').empty().append(Mustache.render(template, {
                                jp_container_id : "jp_container_1",
                                jquery_jplayer_id : "jquery_jplayer_1",
                                title : ""
                        }));
                        $("#jquery_jplayer_1").jPlayer({
                                ready : function() {
                                        self.ready = true;
                                        $(document.documentElement).addClass("jplayer-enabled");
                                },
                                swfPath: "/flash/Jplayer.swf",
                                wmode: "window"
                        });
                        $('#jquery_jplayer_1').bind($.jPlayer.event.error, function(e) {
                                /*jshint onecase:true */
                                var message = "Unknown error encountered";
                                switch (e.jPlayer.error.type)
                                {
                                        case $.jPlayer.error.URL :
                                                message = "Could not load the sample track";
                                                break;
                                }

                                if (self.onerror)
                                {
                                        self.onerror.call(self, message);
                                }
                        });
                },

                load : function(name, url) {
                        if (!this.ready)
                        {
                                return;
                        }
                        $('#jplayer').css('height', '');
                        this.set_title(name);
                        $("#jquery_jplayer_1").jPlayer('setMedia', {
                                mp3: url
                        });
                },
                set_title : function(text) {
                        $('#jplayer .jp-title li').first().text(text);
                },

                play : function() {
                        $("#jquery_jplayer_1").jPlayer('play');
                },
                /* When IE8 goes extinct, we can use a proper setter
                get visible() {
                 */
                visible : function() {
                        return ($('#jplayer').height() !== 0);
                }
        };
});
