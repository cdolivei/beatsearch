/* vim: set expandtab shiftwidth=8 tabstop=8 */
requirejs.config({
        baseUrl : "/javascript",
        paths : {
                jquery : "/foundation/javascripts/jquery",
                modernizer : "/foundation/javascripts/modernizr.foundation",
                foundation : "/foundation/javascripts/app",
                "jquery.foundation.reveal" : "/foundation/javascripts/jquery.foundation.reveal"
        },
        shim : {
                'native.history' : {
                        exports : 'History'
                },
                "foundation" : [ "jquery", "modernizer", "jquery.foundation.reveal" ],
                "jquery.foundation.reveal" : {
                        deps: [ "jquery" ],
                        exports : "jQuery.fn.foundationReveal"
                }
        }
});

require([ "search", 'native.history', 'jplayer', 'underscore', 'QueryString', 'foundation' ], function(Search, History, Jplayer, _, QueryString) {
        document.forms[0].onsubmit = function() {
                var query = $('#search').val();
                var state = new Object;
                state.query = query;
                History.pushState(state, "Search for " + query, "?query=" + query);

                return false;
        };

        function change_page(data)
        {
                _.each(data, function(value, key) {
                        switch (key)
                        {
                                case 'query':
                                        Search.search(value, data.page);
                                        break;
                                case 'artist' :
                                        Search.search_artist_by_id(value);
                                        break;
                                case 'release' :
                                        Search.search_release_by_id(value);
                                        break;
                                case 'track' :
                                        Search.search_track_by_id(value);
                                        break;
                                case 'artist_releases' :
                                        Search.search_releases_by_artist(value, data.page, data.filter);
                                        break;
                                case 'artist_tracks' :
                                        Search.search_tracks_by_artist(value, data.page, data.filter);
                                        break;
                                case 'home' :
                                        $('#content').empty();
                                        $('#pages').hide();
                                        break;
                        }
                });
        }

        History.Adapter.bind(window, 'statechange', function() {
                var state = History.getState();

                change_page(state.data);
        });

        $('#title h1 a').click(function(e) {
                $('#content').empty();
                History.pushState({ home: '' }, "beatsearch", "/");
                e.preventDefault();
                return false;
        });

        $(document).on('click', '.search-tips a', function(e) {
                e.preventDefault();
                $('#search').val($(e.target).text());
                $('#query').click();
        });

        if (document.location.search.length > 0)
        {
                var qs = new QueryString(document.location.search);
                var state = History.getState();

                /* if all the parameters in replaceState are the same (this happens
                 * when the refresh button is hit), a call to replaceState will not
                 * trigger onstatechange. So we must call change_page manually.
                 * Comparing the title should be sufficient for our use cases
                 */
                if (state && state.title == document.title)
                {
                        change_page.call(null, qs.to_object());
                }

                History.replaceState(qs.to_object(), document.title, document.location.search);

                $('#search-hint').removeClass('hide');
        }
        else
        {
                $('#search-tips').removeClass('hide');
        }

        Jplayer.init();
        Jplayer.onerror = function _jplayer_error(message) {
                this.set_title(message);
        };

        if (!History.enabled)
        {
                $('#nohistory').reveal({
                        open : function() {
                                $('#nohistory img').each(function() {
                                        $(this).attr('src', $(this).data('src'));
                                });
                        }
                });
        }
});
