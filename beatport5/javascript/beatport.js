/* vim: set expandtab shiftwidth=8 tabstop=8 */
define([ "jquery", "q", "underscore" ], function($, Q, _) {
        var api_url = "http://api.beatport.com";

        return {
                request : function(path, parameters) {
                        var defer = Q.defer();

                        /* damn you Internet Explorer
                         * IE < 10 needs something special to support Cross-Domain
                         * requests called XDomainRequest
                         * See:
                         * http://www.nczonline.net/blog/2010/05/25/cross-domain-ajax-with-cross-origin-resource-sharing/
                         */
                        if (typeof XDomainRequest !== "undefined")
                        {
                                var request = new XDomainRequest;
                                // :(
                                var query_string = new Array;
                                var get_params = _.extend({
                                        v : "2.0",
                                        format : "json"
                                }, parameters);

                                // yikes
                                _.each(_.keys(get_params), function(key) {
                                        query_string.push(key + "=" + encodeURIComponent(get_params[key]));
                                });

                                // blarg
                                request.open("GET", api_url + path + "?" + query_string.join("&"));
                                request.onerror = function() {
                                        /* onerror could be called for many different reasons. Unfortunately
                                         * there is no way to get a more detailed error message
                                         */
                                        defer.reject("The request to beatport.com failed. Maybe the server is down or the information you are searching for no longer exists");
                                };
                                request.onload = function() {
                                        defer.resolve($.parseJSON(request.responseText));
                                };
                                // http://social.msdn.microsoft.com/Forums/en-US/iewebdevelopment/thread/30ef3add-767c-4436-b8a9-f1ca19b4812e
                                request.onprogress = function() {};
                                request.ontimeout = function() {};
                                request.send();
                                return defer.promise;
                        }
                        $.ajax(api_url + path, {
                                data : _.extend({
                                        v : "2.0",
                                        format : "json"
                                }, parameters),
                                dataType : "json",
                                success : function(data, textStatus, jqXHR) {
                                        if (textStatus == "success")
                                        {
                                                defer.resolve(data);
                                        }
                                        else
                                        {
                                                defer.reject("Request failed. Messaged received: " + textStatus);
                                        }
                                },
                                error : function(jqXHR, textStatus, errorThrown)
                                {
                                        var message = errorThrown || "Unknown error encountered";
                                        switch (textStatus)
                                        {
                                                case "timeout":
                                                        message = "Could not find the beatport server. Please try again later";
                                                        break;
                                                case "error":
                                                        // I only encountered this for DNS resolution problems, but
                                                        // it's probably more generic than that
                                                        break;
                                                case "abort":
                                                        message = "Request to beatport.com has been aborted. Please try again later";
                                                        break;
                                                case "parsererror":
                                                        message = "Got a response from beatport that I wasn't expecting. File a bug!";
                                                        break;
                                        }
                                        defer.reject(message);
                                }
                        });

                        return defer.promise;
                }
        };
});
