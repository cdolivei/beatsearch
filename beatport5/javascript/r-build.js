/* This r.js build file optimizes out javascript
 * You can run it by r.js -o r-build.js
 */

({
	baseUrl : ".",
	paths : {
		jquery : "../foundation/javascripts/jquery",
		modernizer : "../foundation/javascripts/modernizr.foundation",
		foundation : "../foundation/javascripts/app",
		"jquery.foundation.reveal" : "../foundation/javascripts/jquery.foundation.reveal"
	},
	mainConfigFile : "index.js",
	name : "index",
	out : "index.min.js"
})
