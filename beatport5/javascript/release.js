/* vim: set expandtab shiftwidth=8 tabstop=8 */
define([ "jquery", "q", "beatport" ], function($, Q, Beatport) {
        return {
                get : function(id)
                {
                        if (isNaN(parseInt(id, 10)))
                        {
                                throw new Error(query + " is not a number");
                        }
                        return Beatport.request("/catalog/3/releases/detail", {
                                id : id
                        });
                },
                search : function(query, page)
                {
                        return Beatport.request("/catalog/search", {
                                facets : "fieldType:release",
                                query : query,
                                page : page || 1,
                                perPage : 30
                        }).then(function(data) {
                                data.metadata.savedQuery = query;
                                return data;
                        });
                }
        };
});
