/* vim: set expandtab shiftwidth=8 tabstop=8 */
define([ "jquery", "mustache", "underscore", "artist", "track", "release",
         "mixed", 'native.history', 'jplayer', 'q', 'QueryString',
         'foundation' ],
       function($, Mustache, _, Artist, Track, Release, MixedSearch,
                History, Jplayer, Q, QueryString) {

        function build_pages(number, current)
        {
                var output = new Array;
                var template = $('#pagination_template').html();
                var pages = _.range(1, _.min([ 50, number+1 ]));
                var qs = new QueryString(document.location.search);

                _.each(pages, function(element, index) {
                        var page = {
                                index : element,
                                current : (element == current)
                        };

                        qs.extend({ page: element });
                        _.extend(page, {
                                url : qs.flatten(),
                                data : _.map(qs.to_object(), function(value, key) { 
                                        return { 'key' : key, 'value' : value };
                                })
                        });

                        output.push(page);
                });
                $("#pages")
                .empty()
                .show()
                .append(Mustache.render(template, { pages : output }));
        }

        function hide_pages()
        {
                $("#pages").hide();
                return true;
        }

        /* when the user clicks an anchor, we attempt to write to the browser
         * history the resource they are requesting (which in turn, fires
         * an event that loads the resource). But if the browser does not
         * support the HTML5 History API, we call the fallback instead
         * which is just the search feature
         */
        function write_to_history(e, title)
        {
                var data = $(e.delegateTarget).data();
                var query_data = {};
                _.each(data, function(value, key) {
                        key = key.toLowerCase();
                        if (key.substr(0,2) == 'qs')
                        {
                                query_data[key.substr(2)] = value;
                        }
                });

                History.pushState(query_data,
                                  title,
                                  '?'+ _.map(query_data, function(value, key) {
                                          return key + "=" + encodeURIComponent(value);
                                  }).join("&"));
        }


        /* Attaches listeners to the anchor links in #content */
        function attach_listeners()
        {
                $('#content .track_id a,' +
                  '#content .artist_id a,' +
                  '#content .release_id a').click(function(e) {
                        var target = $(e.delegateTarget);
                        write_to_history(e, target.text());
                        e.preventDefault();
                });

                $("#content .artist_tracks a").click(function(e) {
                        var target = $(e.delegateTarget);
                        write_to_history(e, "Tracks by " + target.data('artist-name'));
                        e.preventDefault();
                });
                $("#content .artist_releases a").click(function(e) {
                        var target = $(e.delegateTarget);
                        write_to_history(e, "Releases by " + target.data('artist-name'));
                        e.preventDefault();
                });

                $("#content .jplayer-play").click(function(e) {
                        var url = $(e.delegateTarget).data('sampleurl');
                        var name = $(e.delegateTarget).data('name');
                        var visible = Jplayer.visible();
                        /* calling load will cause the Jplayer to be visible if
                         * it can play the file
                         */
                        Jplayer.load(name, url);
                        /* We check whether the player is visible
                         * One case where it may fail is where the browser doesn't
                         * support the music file and there is no flash fallback.
                         * In that case, we don't want to scroll to "nothing"
                         */
                        if (!visible && Jplayer.visible())
                        {
                                scrollToElement($('#jplayer'))
                                .then(function() {
                                        Jplayer.play();
                                });
                        }
                        else if (Jplayer.visible())
                        {
                                Jplayer.play();
                        }
                        else
                        {
                                $('#cannot_play_music').reveal();
                        }
                        e.preventDefault();
                });

                $("#content .filter-search").submit(function(e) {
                        var element = $(e.delegateTarget);
                        var filter = element.find('input[name=search-filter]').val();
                        element.data('qs-filter', filter);
                        write_to_history(e, "Searching " + filter + " by " + element.data('artist-name'));
                        e.preventDefault();
                });

        }

        function handle_error(message)
        {
                var template = $("#handle_error").html();
                $('#content').empty().append(Mustache.render(template, { message : message }));
        }

        function generic_view(response, template_id)
        {
                var template = $(template_id).html();
                $('#content').empty().append(Mustache.render(template, response));

                attach_listeners();

                $("#response").text(JSON.stringify(response));
        }

        /* Doing a search returns similar structured data to the point where
         * we can actually make a generic version that just loads a different
         * template depending on whether you are searching track/artist/release
         */
        function generic_list(response, template_id, title)
        {
                generic_view(response, template_id);

                if (response.metadata.totalPages > 1)
                {
                        build_pages(response.metadata.totalPages,
                                    response.metadata.page);

                        $("#pages a").click(function(e) {
                                var page_num = $(e.delegateTarget).text();
                                write_to_history(e, title + " (page " + page_num + ")");
                                e.preventDefault();
                        });
                }
                else
                {
                        hide_pages();
                }

                return true;
        }

        function find_resource_name_by_id(resource, id)
        {
                var result = _.find(resource, function(obj) { return obj.id == id; });
                return (result ? result.name : null);
        }

        function find_artist_from_response(response)
        {
                if (response.results.length === 0)
                {
                        return 'null';
                }
                else
                {
                        return find_resource_name_by_id(response.results[0].artists, response.metadata.savedQuery) || "unknown artist";
                }
        }

        function artist_list(response)
        {
                generic_list(response,
                        '#artist_list',
                        'Searching for ' + response.metadata.savedQuery);
        }

        function artist_get(response)
        {
                generic_view(response.results, '#artist_get');
                hide_pages();

                /* If the bio is longer than 3 lines, reduce it to three
                 * and have the option to show more
                 */
                var bio = $('#biography');
                if (bio.length == 1)
                {
                        var bio_height = parseFloat(bio.height());
                        var bio_line_height = bio.css('line-height');
                        if (_.isNaN(bio_height) || _.isNaN(bio_line_height))
                        {
                                // best not to do anything
                                return;
                        }

                        /* So far, all browsers except IE return pixes when calculating
                         * line-height.
                         * This turns out to valid, but doesn't seem to be well
                         * documented. In which case, we must multiply the
                         * line-height by the font-size
                         */
                        var units_regex = new RegExp("(\\D+)$");
                        var units = units_regex.exec(bio_line_height);
                        if (units !== null && units[1] !== 'px')
                        {
                                // my assertion was wrong... jump ship!
                                return;
                        }
                        if (units === null) // no units specified
                        {
                                var bio_font_size = bio.css('font-size');
                                var bio_font_size_units = units_regex.exec(bio_font_size);
                                // IE can return relative units like 'smaller', so make sure
                                // it's in pixels
                                if (bio_font_size_units !== null && bio_font_size_units[1] == 'px')
                                {
                                        bio_line_height = parseFloat(bio_font_size) * parseFloat(bio_line_height);
                                }
                        }

                        // bio_line_height should be in pixels. Remove the px if it's there
                        bio_line_height = parseFloat(bio_line_height);

                        if (bio_height > bio_line_height * 3)
                        {
                                var show_more_template = $('#show_more_bio').html();
                                // we will reduce the size of the biography to 3 lines
                                bio.css('overflow', 'hidden')
                                   .css('height', (bio_line_height * 3) + "px")
                                   .css('position', 'relative');
                                bio.append(show_more_template);
                                bio.find("span.bio-compact a").click(function() {
                                        $('#biography').remove('span.bio-compact').removeAttr('style');
                                });
                        }
                }
        }

        function track_list(response)
        {
                generic_list(response,
                             '#track_list',
                             'Searching tracks called ' + response.metadata.savedQuery);
        }

        function release_list(response)
        {
                generic_list(response,
                             '#release_list',
                             'Searching releases for ' + response.metadata.savedQuery);
        }

        function release_get(response)
        {
                generic_view(response.results, '#release_get');
                hide_pages();
        }

        function track_get(response)
        {
                generic_view(response, '#track_get');
                hide_pages();
        }

        function artist_tracks_list(response)
        {
                var artist_name = find_artist_from_response(response);
                generic_list(response,
                             '#track_list',
                             (artist_name) ? 'Searching tracks by artist ' + artist_name : 'Unknown artist');
        }

        function mixed_list(response)
        {
                var artists = new Array;
                var releases = new Array;
                var tracks = new Array;
                var mixed_template = $("#mixed_list").html();
                var artist_template = $("#artist_list").html();
                var release_template = $("#release_list").html();
                var track_template = $("#track_list").html();

                _.each(response.results, function(element, index) {
                        switch (element.type)
                        {
                                case "artist":
                                        artists.push(element);
                                        break;
                                case "track":
                                        tracks.push(element);
                                        break;
                                case "release":
                                        releases.push(element);
                                        break;
                        }
                });
                var suggestions = false;
                // suggestions is an array if there are no suggestions, and an object if there are suggestions...
                if (!_.isArray(response.metadata.spellcheck.suggestions))
                {
                        var query = response.metadata.savedQuery;
                        var query_array = query.split('');
                        _.each(response.metadata.spellcheck.suggestions, function(value, key) {
                                query_array.splice.apply(query_array, [ value.start, value.end - value.start ].concat(key.split('')));
                        });
                        suggestions = query_array.join('');
                }
                $("#content").empty().append(
                        Mustache.render(mixed_template, {
                                artists_count : artists.length,
                                tracks_count : tracks.length,
                                releases_count : releases.length,
                                suggestions : suggestions
                        }));
                if (artists.length > 0)
                {
                        $("#content").append("<h3 class=row>Artists</h3>");
                        $("#content").append(Mustache.render(artist_template, { results : artists }));
                }
                if (releases.length > 0)
                {
                        $("#content").append("<h3 class=row>Release</h3>");
                        $("#content").append(Mustache.render(release_template, { results : releases }));
                }
                if (tracks.length > 0)
                {
                        $("#content").append("<h3 class=row>Tracks</h3>");
                        $("#content").append(Mustache.render(track_template, { results : tracks }));
                }
                attach_listeners();
                hide_pages();
                
                $("#response").text(JSON.stringify(response));
        }

        function artist_release_list(response)
        {
                var artist_name = find_artist_from_response(response);
                generic_list(response,
                             '#release_list',
                             (artist_name) ? 'Searching releases by artist ' + artist_name : 'Unknown artist');
        }

        function scrollToContent(result)
        {
                return scrollToElement($('#content'));
        }

        function scrollToElement(element) {
                var rect = element.offset();
                rect.width = element.width();
                rect.height = element.height();
                var win = $(window);
                var scrollTo = null;
                var defer = Q.defer();
                if (win.scrollTop() > (rect.top + rect.height) ||
                    win.scrollTop() + win.height() < rect.top)
                {
                        // The order matters. If we have a long element that goes
                        // beyond the viewport, than we want to scroll to the top
                        // rather than scroll to the bottom
                        if (rect.height >= win.height() || rect.top + rect.height < win.scrollTop())
                        {
                                scrollTo = rect.top;
                        }
                        else if (rect.top + (rect.height / 2) > (win.scrollTop() + win.height()))
                        {
                                // scroll down to element where the bottom of the screen shows the bottom of the element
                                scrollTo = rect.top + rect.height - win.height();

                        }
                }

                if (scrollTo !== null)
                {
                        // html for firefox, body for chrome
                        $('html, body').animate({
                                "scrollTop" : scrollTo
                        }, 2000, function () {
                                defer.resolve(true);
                        });
                }
                else
                {
                        defer.resolve(false);
                }
                return defer.promise;
        }

        var Search = {
                search :  function(query, page) {
                        query = $.trim(query);
                        var lower = query.toLowerCase();

                        $('#progress_bar')
                        .clone()
                        .removeClass('hide')
                        .appendTo($('#content').empty());
                        if (lower.indexOf("a:") === 0)
                        {
                                Artist.search(query.substring(2), page)
                                .then(artist_list)
                                .then(scrollToContent)
                                .fail(handle_error);
                        }
                        else if (lower.indexOf("t:") === 0)
                        {
                                Track.search(query.substring(2), page)
                                .then(track_list)
                                .then(scrollToContent)
                                .fail(handle_error);
                        }
                        else if (lower.indexOf("r:") === 0)
                        {
                                Release.search(query.substring(2), page)
                                .then(release_list)
                                .then(scrollToContent)
                                .fail(handle_error);
                        }
                        else
                        {
                                MixedSearch.search(query)
                                .then(mixed_list)
                                .then(scrollToContent)
                                .fail(handle_error);
                        }
                },

                search_track_by_id : function(id) {
                        Track.get(id)
                        .then(track_get)
                        .then(scrollToContent)
                        .fail(handle_error);
                },

                search_artist_by_id : function(id) {
                        Artist.get(id)
                        .then(artist_get)
                        .then(scrollToContent)
                        .fail(handle_error);
                },

                search_release_by_id : function(id) {
                        Release.get(id)
                        .then(release_get)
                        .then(scrollToContent)
                        .fail(handle_error);
                },

                search_releases_by_artist : function(artist_id, page, filter) {
                        Artist.releases_search(artist_id, page, filter)
                        .then(artist_release_list)
                        .then(scrollToContent)
                        .fail(handle_error);
                },

                search_tracks_by_artist : function(artist_id, page, filter) {
                        Artist.tracks_search(artist_id, page, filter)
                        .then(artist_tracks_list)
                        .then(scrollToContent)
                        .fail(handle_error);
                }
        };

        return Search;
});
