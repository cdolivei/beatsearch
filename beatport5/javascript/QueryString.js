/* vim: set expandtab shiftwidth=8 tabstop=8 */
define([ 'underscore' ], function(_) {
        function QueryString(query_string)
        {
                if (!_.isString(query_string))
                {
                        throw new Error("Expected parameter to be of type 'string'");
                }

                if (query_string.length > 0 && query_string[0] == '?')
                {
                        query_string = query_string.substr(1);
                }

                if (query_string.length === 0)
                {
                        this.qs = new Object;
                        return;
                }
                
                var self = this;
                self.qs = new Object;
                _.each(query_string.split('&'), function(data) {
                        // does not handle http array syntax (foo[])
                        var fields = data.split('=');
                        self.qs[fields[0]] = decodeURIComponent(fields[1]);
                });
        }

        QueryString.prototype.to_object = function()
        {
                return _.clone(this.qs);
        };

        QueryString.prototype.flatten = function()
        {
                return _.map(this.qs, function(value, key) {
                        return (key + "=" + encodeURIComponent(value));
                }).join("&");
        };

        QueryString.prototype.extend = function(params)
        {
                if (!_.isObject(params))
                {
                        throw new Error("parameter is not an object");
                }

                this.qs = _.extend(this.qs, params);
                return this;
        };

        return QueryString;
});
