#!/bin/sh

THIS_PATH=$(dirname $(readlink -f $0))
NODE_MODULES=${THIS_PATH}/../node_modules
ROOT=${THIS_PATH}/../beatport5

cd ${ROOT}

rm -v stylesheets/opt.css
cat foundation/stylesheets/*.css stylesheets/*.css | ${NODE_MODULES}/clean-css/bin/cleancss -o stylesheets/opt.css

rm -v javascript/require.min.js javascript/index.min.js
${NODE_MODULES}/uglify-js/bin/uglifyjs javascript/require.js -o javascript/require.min.js
node ${NODE_MODULES}/requirejs/bin/r.js -o javascript/r-build.js
